package com.udonapps.drawermenusample_kotlin

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.google.android.material.navigation.NavigationView
import com.udonapps.drawermenusample_kotlin.R.id.item1
import com.udonapps.drawermenusample_kotlin.R.id.item2
import com.udonapps.drawermenusample_kotlin.R.id.item3
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_drawer.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        init()
    }
    private fun init(){
        val toggle = ActionBarDrawerToggle(Activity(), drawer_layout, toolbar, R.string.nav_open, R.string.nav_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        navigation_view.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            item1->{
                message.text = item.title
            }
            item2->{
                message.text = item.title
            }
            item3->{
                message.text = item.title
            }
            else->{

            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }


}